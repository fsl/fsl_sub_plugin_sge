
# fsl_sub_plugin_sge release history

## 1.5.5

* Add option/environment variable to enable nested queuing of tasks - principally of use when using Open OnDemand desktop environments

## 1.5.4

* Add license file
* Change to release status

## 1.5.3

* Addition of a control for parallel environment support

## 1.5.2

* Documentation changes for migration to conda-based FSL.

## 1.5.1

* Improve operation of usescript submission method

## 1.5.0

* Add passing of extra arguments to Grid Engine
* Prevent re-loading of modules if environment passing has been configured
    Default value of preserve_modules changed to False
* Ensure coprocessor environment module is loaded if specified

## 1.4.8

* Implement documented (but not-implemented) export_vars configuration option

## 1.4.7

* Support environment variables that have an '=' in their value

## 1.4.6

* Don't set time limits on unconfigured queues
* Add support for configured queue names containing host groups

## 1.4.5

* First public release
